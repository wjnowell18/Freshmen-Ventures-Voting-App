import os
from flask import Flask, session, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from ventures.config import config

if config['auth_type'] == 'sso':
    from flask_sso import SSO

app = Flask(__name__)

# Config
BASE_DIR = os.path.dirname(os.path.realpath(__file__))
app.config['BASE_DIR'] = BASE_DIR
app.config['UPLOAD_DIR'] = BASE_DIR + config['upload_dir']
app.config['SQLALCHEMY_DATABASE_URI'] = config['db_uri']
app.config['ALLOWED_EXTENSIONS'] = set(config['upload_ext'])
app.config['MAX_VOTES'] = config['votes_per_user']
app.config['VOTE_ALL_AT_ONCE'] = config['all_at_once']
app.config['USE_WHITELIST'] = config['whitelist']
app.config['ADMIN_VOTE'] = config['admin_vote']
app.config['THUMBNAIL_SIZE'] = (config['thumbnail_size'],
                                config['thumbnail_size'])
app.config['DEBUG'] = config['DEBUG']
app.secret_key = config['secret_key']
app.config['AUTH_TYPE'] = config['auth_type']

# Authentication
if config['auth_type'] == 'sso':
    app.config.setdefault('SSO_ATTRIBUTE_MAP', config['SSO_ATTRIBUTE_MAP'])
    app.config.setdefault('SSO_LOGIN_URL', '/login')
    app.config['ADMINISTRATORS'] = config['admins']

    ext = SSO(app=app)

    @ext.login_handler
    @app.route('/login')
    def login(user_info=None):
        if user_info:
            session['user'] = user_info
            if session['user']['uid']:
                session['user']['username'] = session['user']['uid']
            if session['user']['username'] in config['admins']:
                session['user']['is_admin'] = True
        else:
            flash('SSO Login Error: Check SSO configuration.')

        # Direct to appropriate page based on usertype
        if 'user' in session and 'username' in session['user']:
            if 'is_admin' in session['user']:
                return redirect(url_for('entries_page'))
            else:
                return redirect(url_for('vote_page'))
        else:
            return redirect(url_for('index'))

    @app.route('/logout')
    def logout():
        session.pop('user', None)
        return redirect(url_for('index'))

db = SQLAlchemy(app)

import ventures.views

if __name__ == "__main__":
    app.run()
