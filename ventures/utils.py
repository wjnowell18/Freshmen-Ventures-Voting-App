import os
from datetime import datetime
from PIL import Image
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
import sqlalchemy

from ventures import app, db
import ventures.models as models


def init_db():
    """ Clears database, creates initial tables. """

    db.reflect()
    db.drop_all()
    db.create_all()


def clear_files():
    """ Clears uploads (except .hidden files), returns True if error. """

    file_error = False
    for f in os.listdir(app.config['UPLOAD_DIR']):
        if f.startswith("."):
            continue

        path = os.path.join(app.config['UPLOAD_DIR'], f)
        try:
            if os.path.isfile(path):
                os.unlink(path)
        except:
            file_error = True

    return file_error


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


def get_entries():
    """ Returns tuple (dictionary_of_entries, error_string) """

    # Check if table/dadabase exists.
    error = None
    edict = None
    try:
        q = models.Entry.query.order_by(models.Entry.order).all()
        edict = {'entries': q}

    # Check if database exists:
    except sqlalchemy.exc.OperationalError as err:
        error = str(("EntryError: Database does not exist. \
                     Try creating it first.</br>", err))

    # Check if tables exist
    except sqlalchemy.exc.ProgrammingError as err:
        error = str(("EntryError: Table does not exist.", err))

    return edict, error


def get_votes(username):
    """ Returns a list of IDs for already-cast votes. """

    # Check if table/dadabase exists.
    error = None
    votes = None

    try:
        uv = models.Uservote.query.filter_by(username=username).first()
        votes = []
        if uv and uv.uservotes:
            uv = uv.uservotes.all()
            for vote in uv:
                votes.append(vote.entry_id)

    # Check if database exists:
    except sqlalchemy.exc.OperationalError as err:
        error = str(("VoteError: Database does not exist. \
                     Try creating it first.</br>", err))

    # Check if tables exist
    except sqlalchemy.exc.ProgrammingError as err:
        error = str(("VoteError: Table does not exist.", err))

    return votes, error


def save_image(image):
    """ Saves image (string or werkzeug FileStorage), returns filename. """

    if isinstance(image, str) or isinstance(image, bytes):
        file = FileStorage(open(image, 'rb'))
    else:
        file = image

    filename = datetime.utcnow().strftime("%Y%m%d%H%M%S")
    filename += "." + secure_filename(file.filename).split(".")[-1]
    savepath = os.path.join(app.config['UPLOAD_DIR'],
                            os.path.basename(filename))

    # Rename in case of filename conflict
    i = 0
    newfilename = ""
    while os.path.isfile(savepath):
        if i == 0:
            fileext = filename.split(".")[-1]
            filename = ".".join(filename.split(".")[:-1])

        i += 1

        newfilename = filename + "-" + str(i) + "." + fileext

        savepath = os.path.join(app.config['UPLOAD_DIR'], newfilename)

    if newfilename:
        filename = newfilename

    file.save(savepath)

    # Generate thumbnail
    im = Image.open(savepath)
    im.thumbnail(app.config['THUMBNAIL_SIZE'])

    try:
        im.save(savepath + ".thumb", "JPEG")
    except IOError:
        im.convert('RGB').save(savepath + ".thumb", "JPEG")

    return filename


def rm_image(entry):
    """ Removes image and thumbnail associated with entry. """

    os.remove(os.path.join(app.config['UPLOAD_DIR'], entry.image))
    os.remove(os.path.join(app.config['UPLOAD_DIR'], entry.image + '.thumb'))


def add_entry(name, description, image, order=-1):
    """ Adds entry to database, and handles image uploading and thumbnails.

    name, description -- strings saved to db
    image -- either a filename, or a werkzeug FileStorage object
    """

    if order == -1:
        last_entry = models.Entry.query.order_by(
                    models.Entry.order.desc()).first()
        order = last_entry.order + 1

    filename = save_image(image)

    entry = models.Entry(name, description, filename, order)

    db.session.add(entry)
    db.session.commit()


def rm_entry(to_del):
    """ Removes an entry by ID and the image+thumbnail associated with it. """

    if not isinstance(to_del, list):
        to_del = [to_del]

    to_del_query = []
    for entry_id in to_del:
        to_del_query.append(models.Entry.query.get(entry_id))

    for entry in to_del_query:
        rm_image(entry)
        for vote in entry.entryvotes.all():
            db.session.delete(vote)
        db.session.delete(entry)

    if to_del_query:
        db.session.commit()


def debug_populate():
    """ Creates an entry for each image in ./testdata """

    index = 0
    lst = []
    datadir = os.path.join(app.config['BASE_DIR'], "testdata")
    for f in os.listdir(datadir):
        lst.append(f)
        index += 1
        if f.startswith("."):
            continue

        if f.split(".")[-1] in app.config['ALLOWED_EXTENSIONS']:
            add_entry("Entry " + str(index), "Desc " + str(index),
                      os.path.join(datadir, f), index)

    return lst


def update_entry_order():
    """ Iterates over entries, assigning an order to any unordered entries. """

    pass


def add_voters(csv):
    """ Takes a set of comma separated usernames, and adds to database. """

    if isinstance(csv, str) and csv:
        csv = csv.split(',')
    else:
        return None

    for username in csv:
        username = username.strip()

        # Dedupe, don't add voters who are already whitelisted.
        dupe_voter = models.Uservote.query.filter_by(username=username).first()
        if dupe_voter:
            continue
        else:
            username = models.Uservote(username)
            db.session.add(username)

    db.session.commit()

    return True


def rm_all_voters(delete_votes=True):
    """ Removes ALL voters and their votes. """

    for voter in models.Uservote.query.all():
        if delete_votes:
            for vote in voter.uservotes:
                db.session.delete(vote)

        db.session.delete(voter)

    db.session.commit()


def rm_voters(csv, delete_votes=True):
    """ Removes voters from a comma-separated list of usernames. """

    if isinstance(csv, str) and csv:
        csv = csv.split(',')
    else:
        return None

    for username in csv:
        username = username.strip()

        voter = models.Uservote.query.filter_by(username=username).first()
        if not voter:
            continue

        if delete_votes:
            for vote in voter.uservotes.all():
                db.session.delete(vote)

        db.session.delete(voter)

    db.session.commit()


def get_voters():
    """ Returns a string of usernames, separated by commas. """

    # Check if table/dadabase exists.
    error = None
    voters = ""
    try:
        q = models.Uservote.query.all()
        for voter in q:
            voters += voter.username + ", "
        voters = voters[:-2]

    # Check if database exists:
    except sqlalchemy.exc.OperationalError as err:
        error = str(("VoterError: Database does not exist. \
                     Try creating it first.</br>", err))

    # Check if tables exist
    except sqlalchemy.exc.ProgrammingError as err:
        error = str(("VoterError: Table does not exist.", err))

    return voters, error


def clear_votes():
    for vote in models.Vote.query.all():
        db.session.delete(vote)

    db.session.commit()


def is_eligible(username):
    if isinstance(username, str):
        if models.Uservote.query.filter_by(username=username).first():
            return True
        else:
            return False
    else:
        return None
