from ventures import db


class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.Integer)
    name = db.Column(db.String(120))
    desc = db.Column(db.Text)
    image = db.Column(db.String(255))

    def __init__(self, name, desc, image, order):
        self.name = name
        self.desc = desc
        self.image = image
        self.order = order

    def __repr__(self):
        return '<Entry %r>' % self.name


# Stores username and each item user voted for.
class Uservote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120))
    votes = db.relationship('Vote', backref='voter', lazy='dynamic')

    def __init__(self, username):
        self.username = username

    def __repr__(self):
        return '<UserVote %r>' % self.username


# The individual vote object.
class Vote(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    entry_id = db.Column(db.Integer, db.ForeignKey('entry.id'))
    entry = db.relationship('Entry',
        backref=db.backref('entryvotes', lazy='dynamic'),
        primaryjoin="Vote.entry_id==Entry.id")

    uservote_id = db.Column(db.Integer, db.ForeignKey('uservote.id'))
    uservote = db.relationship('Uservote',
        backref=db.backref('uservotes', lazy='dynamic'),
        primaryjoin="Vote.uservote_id==Uservote.id")

    def __init__(self, uservote, entry):
        self.uservote = uservote
        self.entry = entry

    def __repr__(self):
        return '<Vote %r>' % self.id
