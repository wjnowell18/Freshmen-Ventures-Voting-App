import sys, os
sys.path.append(os.path.dirname(__file__))
import ventures
application = ventures.app # mod_wsgi looks for 'application' specifically.

if __name__ == "__main__":
    if "-c" in sys.argv:
        ventures.utils.init_db()
