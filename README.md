# Freshmen Ventures Voting App

---

### Synopsis

Annually, the Sally McDonnell Barksdale Honors College has a program called Freshmen Ventures, where groups of freshmen apply for the opportunity to travel and explore a topic of their choice around a theme provided by the Honors College. The culmination of this opportunity is a retreat where all groups get together and present what they did, generally through a video that they produced. After all of the presentations, everyone votes on their favorite three groups. The Honors College is seeking an app to remove the manual voting and tallying process for this event, and allow every student to vote online.

### Scope

The purpose of the app will be to manage all of the entries, provide a voting mechanism, and then vote results. It will need a management section for a manager to upload and update entries, which will include an image, title, group members, and presentation order. The management section will also need a vote result page, which will provide a breakdown of which groups got how many votes. There will also be a student section which will list all of the entries which were added in the management section. All of this, the student section especially, will have to be mobile friendly, as most all students will be using their cellphones.


I think the flow is the following:

* Person visits the app page which takes them to a login page
* Person logs in with OleMiss webid credentials
* Based on if the person is a student or manager, they get different pages

**Student**
* A list of all the groups entries. UI/UX to be determined.
* Each entry has a vote checkbox
* JS error check to limit vote number
* Cast Vote/Reset button at the bottom
* Cast Vote/Reset button will need a confirmation
* Once a student votes, the Cast Vote/Reset option goes away

**Manager**
* New entry fields: Screenshot upload, Title, Group Members, Presentation Order
* List existing entries with edit option
* Results Page that lists each entry title and the number of votes received.

### Software Requirements
* Apache
* MySQL or Redis
* PHP
* JS
* Foundation
* A PHP Framework
* OleMiss WebID Authentication
* Git
